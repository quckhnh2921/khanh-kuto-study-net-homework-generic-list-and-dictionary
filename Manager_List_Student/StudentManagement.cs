﻿using System.Text;
namespace Manager_List_Student
{
    public class StudentManagement
    {
        List<Student> students;
        List<Student> listStudentFindingByName;
        public StudentManagement()
        {
            students = new List<Student>();
        }

        public void AddStudent(Student student)
        {
            students.Add(student);
        }

        public void RemoveStudent(string name)
        {
            var student = students.Find(x => x.Name.Equals(name));
            students.Remove(student!);
        }

        public string GetAllStudent()
        {
            StringBuilder sb = new StringBuilder();
            if(students.Count == 0)
            {
                throw new Exception("Empty list");
            }
            foreach (var student in students)
            {
                sb.Append($"\nName: {student.Name} | Age: {student.Age} | Gpa: {student.Gpa}");
            }
            return sb.ToString();
        }

        public void FindStudentByName(string name)
        {
            listStudentFindingByName = new List<Student>();
            foreach (var student in students)
            {
                if (student.Name.Equals(name))
                {
                    listStudentFindingByName.Add(student);
                }
            }
            PrintStudentByName();
        }
        private void PrintStudentByName()
        {
            if (listStudentFindingByName.Count == 0)
            {
                throw new Exception("Student does not exist");
            }
            foreach (var student in listStudentFindingByName)
            {
                Console.WriteLine($"\nName: {student.Name} | Age: {student.Age} | GPA: {student.Gpa}");
            }
        }



    }
}
