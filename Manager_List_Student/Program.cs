﻿using Manager_List_Student;
StudentManagement manager = new StudentManagement();
Menu menu = new Menu();
int choice = 0;
do
{
    try
    {
        menu.MenuDisplay();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Student student = new Student();
                Console.WriteLine("Enter name: ");
                student.Name = Console.ReadLine();
                Console.WriteLine("Enter age: ");
                student.Age = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter gpa: ");
                student.Gpa = double.Parse(Console.ReadLine());
                manager.AddStudent(student);
                break;
            case 2:
                Console.WriteLine("Enter name to remove: ");
                string removeName = Console.ReadLine();
                manager.RemoveStudent(removeName);
                break;
            case 3:
                Console.WriteLine(manager.GetAllStudent());
                break;
            case 4:
                Console.WriteLine("Enter name to find: ");
                string findByName = Console.ReadLine();
                manager.FindStudentByName(findByName);
                break;
            case 5:
                Console.WriteLine("Bye Bye !");
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 5");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 5);