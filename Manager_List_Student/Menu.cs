﻿namespace Manager_List_Student
{
    public class Menu
    {
        public void MenuDisplay()
        {
            Console.WriteLine("==========MENU============");
            Console.WriteLine("| 1. Add student         |");
            Console.WriteLine("| 2. Remove student      |");
            Console.WriteLine("| 3. Display students    |");
            Console.WriteLine("| 4. Find student by name|");
            Console.WriteLine("| 5. Exit                |");
            Console.WriteLine("--------------------------");
            Console.Write("Choice: ");
        }
    }
}
