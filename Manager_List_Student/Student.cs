﻿namespace Manager_List_Student
{
    public class Student
    {
        private string? name;
        private int age;
        private double gpa;

        public string Name
        {
            get => name!;
            set => name = value;
        }
        public int Age
        {
            get => age;
            set
            {
                if(value < 0 || value > 100)
                {
                    throw new Exception("Age is invalid");
                }
                age = value;
            }
        }
        public double Gpa
        {
            get => gpa;
            set
            { 
                if(value < 0 || value > 10)
                {
                    throw new Exception("GPA is invalid");
                }
                gpa = value;
            }
        }
    }
}
