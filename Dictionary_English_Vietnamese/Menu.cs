﻿namespace Dictionary_English_Vietnamese
{
    public static class Menu
    {
        public static void MenuDisplay()
        {
            Console.WriteLine("============Menu============");
            Console.WriteLine("| 1. Add new word          |");
            Console.WriteLine("| 2. Find word             |");
            Console.WriteLine("| 3. Edit word             |");
            Console.WriteLine("| 4. Remove word           |");
            Console.WriteLine("| 5. Print word list       |");
            Console.WriteLine("| 6. Exit                  |");
            Console.WriteLine("============================");
            Console.Write("Choose: ");
        }

        public static void PrintMenu()
        {
            Console.WriteLine("=========Print=========");
            Console.WriteLine("| 1. Print all        |");
            Console.WriteLine("| 2. Only English     |");
            Console.WriteLine("| 3. Only Vietnamese  |");
            Console.WriteLine("| 4. Exit             |");
            Console.WriteLine("=======================");
            Console.Write("Choose: ");
        }

        public static void EditMenu()
        {
            Console.WriteLine("======Edit======");
            Console.WriteLine("| 1. Key       |");
            Console.WriteLine("| 2. Value     |");
            Console.WriteLine("| 3. Key, value|");
            Console.WriteLine("| 4. Exit      |");
            Console.WriteLine("================");
            Console.Write("Choose: ");
        }
    }
}
