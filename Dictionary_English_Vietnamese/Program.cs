﻿using Dictionary_English_Vietnamese;
DictionaryManagement management = new DictionaryManagement();
int choice = 0;
do
{
    try
    {
        Menu.MenuDisplay();
        choice = int.Parse(Console.ReadLine()!);
        switch (choice)
        {
            case 1:
                Word word = new Word();
                Console.WriteLine("Enter english: ");
                word.English = Console.ReadLine()!;
                Console.WriteLine("Enter Vietnamese");
                word.Vietnamese = Console.ReadLine()!;
                management.AddNewWord(word);
                break;
            case 2:
                Console.WriteLine("Enter word to find: ");
                var wordFind = management.GetWord(Console.ReadLine()!);
                Console.WriteLine(wordFind);
                break;
            case 3:
                int editChoice = 0;
                do
                {
                    Menu.EditMenu();
                    editChoice = int.Parse(Console.ReadLine()!);
                    switch (editChoice)
                    {
                        case 1:
                            Console.WriteLine("Enter key to edit: ");
                            var key = Console.ReadLine();
                            management.EditKey(key!);
                            break;
                        case 2:
                            Console.WriteLine("Enter value to edit: ");
                            var value = Console.ReadLine();
                            management.EditValue(value!);
                            break;
                        case 3:
                            Console.WriteLine("Enter word to edit: ");
                            var wordEdit = Console.ReadLine();
                            management.EditKeyAndValue(wordEdit!);
                            break;
                        default:
                            break;
                    }
                } while (editChoice < 3);
                break;
            case 4:
                Console.WriteLine("Enter word to remove: ");
                var wordRemove = Console.ReadLine();
                management.RemoveWord(wordRemove!);
                break;
            case 5:
                int printChoice = 0;
                do
                {
                    Menu.PrintMenu();
                    printChoice = int.Parse(Console.ReadLine()!);
                    switch (printChoice)
                    {
                        case 1:
                            Console.WriteLine(management.DisplayAllKeyAndValue());
                            break;
                        case 2:
                            Console.WriteLine(management.DisplayAllKey());
                            break;
                        case 3:
                            Console.WriteLine(management.DisplayAllValue());
                            break;
                        default:
                            break;
                    }
                } while (printChoice < 4);
                break;
            case 6:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 5");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 6);